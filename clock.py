#!/usr/bin/env python3
import PySimpleGUI as sg
import time
sg.change_look_and_feel('DarkAmber')    # Add a touch of color
# All the stuff inside your window.
parts = {}
parts['time'] = sg.Text('00:00',
                    auto_size_text=True,
                    font=('Helvetica Bold',30),
                    justification='left',
                    key='time')
parts['date'] = sg.Text('1999 December 11',
                    auto_size_text=True,
                    font=('Helvetica Bold', 9),
                    justification='left',
                    key='date')
parts['day'] = sg.Text('NNNNNNNNNN',
                    auto_size_text=True,
                    font=('Helvetica Bold', 9),
                    justification='left',
                    key='day')
ejnye= [[parts['date']], [parts['day']]]
layout = [[
    parts['time'],
    sg.Column(ejnye)],
    [sg.Graph(
        canvas_size=(450, 230),
        graph_bottom_left=(0,0),
        graph_top_right=(450,100),
        #background_color='black',
        pad = ((0,0),(0,0)),
        key='graph',
        tooltip='Chart')
    ]]
# Create the Window
window = sg.Window('Clock', layout, 
        location=(0,0), 
        size=(480,320), 
        margins=(5,5),
        element_padding=((1,1),(1,1)),
        keep_on_top=True, 
        alpha_channel=.5, 
        finalize=True)
window.Maximize()
graph = window['graph']
frame = graph.draw_rectangle((1,0),(449,99), line_color='green')
# Event Loop to process "events" and get the "values" of the inputs
while True:
    event, values = window.read(timeout=10)
    window['time'].update(time.strftime('%H:%M'))
    window['date'].update(time.strftime('%Y %B %d'))
    window['day'].update(time.strftime('%A'))
    sec=time.localtime(time.time()).tm_sec
    try:
        graph.DeleteFigure(arrow)
    except NameError:
        print('No arrow yet')
    arrow = graph.DrawLine((120,0),(sec*4,50), color='white')
    time.sleep(0.49)
window.close()
