#!/usr/bin/env python3
from subprocess import Popen, PIPE
from collections import deque
print("Hi!")
cmd = ['/usr/sbin/collectd', '-f', '-C', 'cddump.conf']
collectd = Popen(cmd, bufsize=1, stdout=PIPE, 
        stderr=PIPE, universal_newlines=True)
print(collectd.pid)
n = 0
charts = dict()
while True:
    print('.',)
    line = collectd.stdout.readline()
    if line.startswith('PUTVAL'):
        n = n+1
        elements = line.split()
        names = elements[1].split('/')
        data = elements[3].split(':')
        if names[1].startswith('aggregation'):
            host = names[0]
            name = names[2]
            valu =  data[1]
            if host not in charts:
                charts[host] = dict()
            if name not in charts[host]:
                charts[host][name] = deque('', 3)
            charts[host][name].append(valu)
            print('\n',n,host,name,valu,charts)
collectd.kill()
